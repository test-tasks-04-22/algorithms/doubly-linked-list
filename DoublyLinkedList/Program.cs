﻿using DoublyLinkedList;

var linkedList = new DoublyLinkedList<string>();

linkedList.Add("1");
linkedList.Add("2");
linkedList.Add("3");
linkedList.Add("4");
linkedList.AddFirst("0");

foreach(var item in linkedList)
{
    Console.WriteLine($"{item} добавлено в список");
}

linkedList.Remove("1");
Console.Write("Список после удаления элемента: ");
foreach (var item in linkedList)
{
    Console.Write($"{item} ");
}

var isPresent = linkedList.Contains("3");
Console.Write("\n");
Console.WriteLine(isPresent == true ? "3 присутствует" : "3 отсутствует");

Console.Write("Список в обратном порядке: ");
foreach (var item in linkedList.BackEnumerator())
{
    Console.Write($"{item} ");
}

linkedList.AddFirst("10");
Console.Write("\nСписок после добавления нового элемента: ");
foreach (var item in linkedList)
{
    Console.Write($"{item} ");
}
