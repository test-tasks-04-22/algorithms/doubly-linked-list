﻿using System.Collections;
using System.Diagnostics;

namespace DoublyLinkedList;

public class DoublyNode<T>
{
    public DoublyNode(T data)
    {
        Data = data;
    }
    public T Data { get; }
    public DoublyNode<T>? Previous { get; set; }
    public DoublyNode<T>? Next { get; set; }
}

public class DoublyLinkedList<T> : IEnumerable<T> 
{
    private DoublyNode<T>? _head;
    private DoublyNode<T>? _tail;

    public void Add(T data)
    {
        var node = new DoublyNode<T>(data);

        if (_head == null)
            _head = node;
        else
        {
            Debug.Assert(_tail != null, nameof(_tail) + " != null");
            _tail.Next = node;
            node.Previous = _tail;
        }
        _tail = node;
        Count++;
    }
    public void AddFirst(T data)
    {
        var node = new DoublyNode<T>(data);
        var temp = _head;
        node.Next = temp;
        _head = node;
        if (Count == 0)
            _tail = _head;
        else
        {
            Debug.Assert(temp != null, nameof(temp) + " != null");
            temp.Previous = node;
        }
        Count++;
    }

    public bool Remove(T data)
    {
        var current = _head;
        
        while (current != null)
        {
            Debug.Assert(current.Data != null, "current.Data != null");
            if (current.Data.Equals(data))
            {
                break;
            }
            current = current.Next;
        }

        if (current == null) return false;
        if(current.Next!=null)
        {
            current.Next.Previous = current.Previous;
        }
        else
        {
            _tail = current.Previous;
        }
            
        if(current.Previous!=null)
        {
            current.Previous.Next = current.Next;
        }
        else
        {
            _head = current.Next;
        }
        Count--;
        return true;
    }

    private int Count { get; set; }
    public bool IsEmpty => Count == 0;

    public void Clear()
    {
        _head = null;
        _tail = null;
        Count = 0;
    }

    public bool Contains(T data)
    {
        var current = _head;
        while (current != null)
        {
            Debug.Assert(current.Data != null, "current.Data != null");
            if (current.Data.Equals(data))
                return true;
            current = current.Next;
        }
        return false;
    }
     
    IEnumerator IEnumerable.GetEnumerator()
    {
        return ((IEnumerable)this).GetEnumerator();
    }

    IEnumerator<T> IEnumerable<T>.GetEnumerator()
    {
        var current = _head;
        while (current != null)
        {
            yield return current.Data;
            current = current.Next;
        }
    }

    public IEnumerable<T> BackEnumerator()
    {
        var current = _tail;
        while (current != null)
        {
            yield return current.Data;
            current = current.Previous;
        }
    }
}
